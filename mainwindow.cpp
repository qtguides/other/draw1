#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<QMouseEvent>
#include<QPainter>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    dibujando = false;
    firstPoint.setX(0);
    firstPoint.setY(0);
    lastPoint.setX(0);
    lastPoint.setY(0);
    penWidth = 20;
    penColor = Qt::red;
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        firstPoint = event->pos();
        dibujando = true;
    }
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    if ((event->buttons() & Qt::LeftButton) && dibujando)
    {
        lastPoint = event->pos();
        update();
    }
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && dibujando)
    {
        lastPoint = event->pos();
        dibujando = false;
        update();
    }
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setPen(QPen(penColor, penWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    painter.drawLine(firstPoint, lastPoint);
    painter.setRenderHint(QPainter::Antialiasing, true);
    QWidget::paintEvent(event);
}
